require 'ircbot/version'

require 'ircbot/logging'

require 'ircbot/connection/connection'

require 'ircbot/message/abstract_message'
require 'ircbot/message/ping'
require 'ircbot/message/notice'
require 'ircbot/message/private'
require 'ircbot/message/notice'
require 'ircbot/message/message'

require 'ircbot/hook/hook'
require 'ircbot/hook/connection'
require 'ircbot/hook/message'
require 'ircbot/hook/notice'

#Dir.glob("#{File.dirname(__FILE__)}/ircbot/message/*.rb").each { |f| puts f }

module Ircbot
  def self.start
    options = {
      server: 'localhost',
      nickname: 'bob'
    }
    Ircbot::Connection.connect options
  end

  def self.logger
    Ircbot::Logging.logger
  end
end
