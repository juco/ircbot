module Ircbot
  module Hook
    module Connection
      CONNECTED = :connected
      DISCONNECTION = :disconnected
      AUTHENTICATION_SENT = :authentication_sent
    end
  end
end
