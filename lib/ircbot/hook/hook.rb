module Ircbot
  module Hook
    class << self

      attr_accessor :hooks

      def register(event, &block)
        @hooks = @hooks || []
        @hooks.push event: event, block: block
      end

      def unregister(event)
        @hooks.reject! { |item| item[:event] == event }
      end

      def trigger(event, data)
        Ircbot.logger "Triggering #{event.to_s}"

        matches = @hooks.take_while do |item|
          item[:event] == event
        end

        matches.each { |match| match[:block].call }
      end
    end
  end
end
