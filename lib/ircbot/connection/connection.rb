require 'socket'
require 'timeout'

module Ircbot

  # Begin, end and maintain a connection with the server
  class Connection

    DEFAULT_OPTIONS = {
      server: '127.0.0.1',
      port: 6667,
      nickname: 'juco',
      username: 'Julian Cohen',
      authentication_delay: 5
    }

    # Convenience method for instantiating and connecting to the IRC server
    #
    # @param options [Hash] The options, including `server`, `port`, `nickname`
    #   and `username
    def self.connect(options = {})
      new(options).connect
    end

    # Send a raw line to the server
    #
    # @param line [String] The raw text to send
    def self.send_raw line
      @@socket.sendmsg line
    end

    def initialize(options = {})
      options = DEFAULT_OPTIONS.merge options

      @server               = options[:server]
      @port                 = options[:port]
      @nickname             = options[:nickname]
      @username             = options[:username]
      @authentication_delay = options[:authentication_delay]
      @status               = :disconnected
    end

    # Create the connection to the IRC server. The connect method
    # uses a [TCPSocket] to do this.
    # #connect also spawns a new thread on which to listen to incoming
    # messages from the server.
    def connect
      @status = :connecting
      Ircbot::logger.info "Connecting to #@server (#{@port})..."

      @@socket = TCPSocket.new @server, @port

      authenticate!

      # TODO: Should probably be passing the socket as an arg
      Thread.new do
        listen
      end.join
    end

    def listen
      while line = @@socket.gets
        received line
      end
    end

    # Called once a message is received from the server.
    #
    # @param message [String] The message
    def received(message)
      begin
        Ircbot.logger.info "Received #{message}"
        m = Message.init(message)
        m.handle
      rescue Message::IgnoredMessageError
        Ircbot.logger.warn "Ignored message: #{message}"
      rescue Message::UnknownMessageError
        Ircbot.logger.warn "Unknown message: #{message}"
      end
    end

    # Perform authentication with the IRC server.
    # This will trigger a {Hook::Connection::AUTHENTICATION_SENT} hook
    def authenticate!
      Ircbot.logger.info('Starting authenticate!')
      Thread.new do
        sleep @authentication_delay
        Ircbot.logger.info 'Authenaticating...'
        @@socket.puts "NICK #@nickname"
        @@socket.puts "USER #@nickname 0 * :#@username"
        Hook.trigger Hook::Connection::AUTHENTICATION_SENT, nick: @nickname, username: @username
      end
    end
  end

  def validate_connection_options!(options)
    keys = [:server, :port, :nickname, :username]
    raise 'Invalid connection options' unless options.keys.eql? keys
  end

end
