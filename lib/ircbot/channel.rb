module Ircbot
  class Channel

    def self.join(name)
      Ircbot.connection.send_raw "JOIN #{name}"
      new(name)
    end

    def initialize(name)
      @name = name
    end

  end
end
