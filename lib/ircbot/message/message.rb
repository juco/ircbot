module Ircbot
  module Message

    MSG_REGEX = /:([\w|\.]+)\s(\d+)\s([\w\\-]+)(.*)/
    PING_REGEX = /PING\s:([\w|\.]+)/

    @message_map = {
      Ircbot::Message::Notice  => [266, 372, 376],
      Ircbot::Message::Private => [400]
    }

    def self.init message
      unless valid? message
        raise IgnoredMessageError.new "Invalid message #{message}"
      end

      return Ping.new(message) if is_ping? message

      parts = parts message
      result = @message_map.keep_if do |klass, codes|
        codes.include? parts[:code]
      end

      raise UnknownMessageError.new "Unknown message #{message}" if result.empty?

      result.keys.first.new parts
    end

    def self.parts message
      matches = message.match /^:(.+)\s(\d+)\s(\w+)\s:?(.*)/
      parts = Hash[[:server, :code, :username, :message].zip matches.to_a[1..4]]
      parts[:code] = parts[:code].to_i
      parts
    end

    private
    def self.valid? str
      res = str =~ MSG_REGEX || str =~ PING_REGEX
      !!res
    end

    def self.is_ping? str
      str.index(/PING/) == 0
    end

    class IgnoredMessageError < StandardError; end;
    class UnknownMessageError < StandardError; end;
  end
end
