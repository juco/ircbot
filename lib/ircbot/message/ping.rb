module Ircbot
  module Message
    class Ping < AbstractMessage

      def handle
        puts @parts.inspect
        Ircbot.logger.info 'PONG!'
        Connection.send_raw "PONG #{@parts[:server]}"
      end

    end
  end
end
