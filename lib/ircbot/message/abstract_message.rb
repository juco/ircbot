module Ircbot
  module Message
    class AbstractMessage

      def initialize parts
        @parts = parts
      end

      def handle
        Ircbot.logger.error "Unhandled message #{self.class.to_s}"
      end

    end
  end
end
