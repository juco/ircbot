require 'logger'

module Ircbot
  class Logging
    def self.initialize_logger(out = STDOUT)
      @logger = Logger.new(out)
      @logger
    end

    def self.logger
      if defined? @logger then @logger else initialize_logger end
    end

    def self.logger=(logger)
      @logger = logger
    end

    def self.level=(level)
      logger.level = level
    end
  end
end
