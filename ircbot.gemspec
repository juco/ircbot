# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ircbot/version'

Gem::Specification.new do |spec|
  spec.name          = "ircbot"
  spec.version       = Ircbot::VERSION
  spec.authors       = ["Julian Cohen"]
  spec.email         = ["julian@juco.co.uk"]
  spec.description   = %q{An awesome IRC bot}
  spec.summary       = %q{An IRC bot built to be extendable with plugins}
  spec.homepage      = "https://github.com/juco/rubot"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "pry"
  spec.add_development_dependency "pry-byebug"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "rspec-mocks"
  spec.add_development_dependency "rspec-nc"
  spec.add_development_dependency "guard"
  spec.add_development_dependency "guard-rspec"
end
