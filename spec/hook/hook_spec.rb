require 'spec_helper'

describe 'Hook module' do
  it 'should allow for registering hooks' do
    Ircbot::Hook.register :foo do
      puts 'foo'
    end
    expect(Ircbot::Hook.hooks.length).to eq 1
  end

  it 'should allow for the removal of hooks' do
    Ircbot::Hook.unregister :foo
    puts Ircbot::Hook.hooks.inspect
    expect(Ircbot::Hook.hooks.length).to eq 0
  end

  it 'should trigger hook events' do
    count = 0
    Ircbot::Hook.register Ircbot::Hook::Notice::USER do |data|
      count = 1
      puts "Received #{data.inspect}"
    end
    Ircbot::Hook.trigger(
      Ircbot::Hook::Notice::USER,
      user: 'foo', text: 'Hello world')
    expect(count).to eq 1
  end

  it 'should do nothing when unregistered events are triggered' do
    Ircbot::Hook.trigger Ircbot::Hook::Notice::CHANNEL, user: 'foo', text: 'moo'
  end
end
