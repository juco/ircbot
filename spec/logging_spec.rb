require 'spec_helper'

describe 'Logging' do

  it '.logger should be a ::Logging' do
    Ircbot::Logging.logger.info 'foo'
  end

  it 'should be possible to set a log level' do
    Ircbot::Logging.level = Logger::WARN
  end

  #logger = nil

  #before :each do
  #  logger = double('myLogging', info: nil, warn: nil)
  #end

  #it 'should instantiate a ::Logging the first time' do
  #  expect(::Logging).to receive(:new).and_return logger
  #  Ircbot::Logging.info('hello world')
  #end

  ## Info
  #it 'should log info messages' do
  #  expect(::Logging).to receive(:new).and_return logger
  #  expect(logger).to receive(:info)
  #  Ircbot::Logging.info 'foo'
  #end

  #it 'should not log info messages when the level is set to warn' do
  #  expect(::Logging).to receive(:new).and_return logger
  #  Ircbot::Logging.level = Ircbot::Logging::WARN
  #  expect(logger).to_not have_received(:info)
  #  Ircbot::Logging.info 'bar'
  #end

  #it 'should not log info when off' do
  #  expect(::Logging).to receive(:new).and_return logger
  #  Ircbot::Logging.level = Ircbot::Logging::OFF
  #  expect(logger).to_not have_received(:info)
  #  Ircbot::Logging.info 'dont log me!'
  #end

  #it 'should log warnings' do
  #  expect(::Logging).to receive(:new).and_return logger
  #  Ircbot::Logging.level = Ircbot::Logging::WARN
  #  expect(logger).to receive(:warn)
  #  Ircbot::Logging.warn 'Big warning'
  #end

  #it 'should log errors when set to warn' do
  #  expect(::Logging).to receive(:new).and_return logger
  #  Ircbot::Logging.level = Ircbot::Logging::ERROR
  #  expect(logger).to receive(:error)
  #  Ircbot::Logging.error('Uh oh!!')
  #end
end
