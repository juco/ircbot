require 'spec_helper'

describe 'Connection' do

  it 'should use the default options for those not specified' do
    connection = Ircbot::Connection.new username: 'Fred'
    expect(connection.instance_variable_get(:@port)).to eq 6667
  end

  it 'should initialize a new socket on .connect()' do
    socket = double('socket', gets: nil)
    thread = double('thread', join: nil)
    expect(TCPSocket).to receive(:new).and_return socket
    expect(Thread).to receive(:new).twice.and_return thread
    Ircbot::Connection.connect
  end

end
