require 'spec_helper'

describe 'Message module' do
  let(:server_message) { ':penguin.omega.org.za 266 juco :Current Global Users: 2  Max: 2' }
  let(:ping_message) { 'PING :penguin.omega.org.za' }
  let(:start_message) { ':penguin.omega.org.za NOTICE Auth :Welcome to Omega!' }

  describe 'valid message?' do
    it 'should return valid for PING\'s' do
      expect(Ircbot::Message.valid? 'PING :foo.bar.moo').to eq true
    end

    it 'should return false for messages without a code' do
      puts "Checking #{start_message}"
      expect(Ircbot::Message.valid? start_message).to eq false
    end

    it 'should return true for server notices' do
      expect(Ircbot::Message.valid? server_message).to eq true
    end
  end

  describe 'is_ping?' do
    it 'should return the Ping class for ping messages' do
      expect(Ircbot::Message::is_ping?(ping_message)).to eq true
    end

    it 'should not return the Ping class for non-pings' do
      expect(Ircbot::Message::is_ping?(server_message)).to eq false
    end
  end

  describe 'init' do
    it 'to return the correct class' do
      expect(Ircbot::Message.init(server_message)).to be_a Ircbot::Message::Notice
      expect(Ircbot::Message.init(ping_message)).to be_a Ircbot::Message::Ping
    end

    it 'should raise an IgnoredMessageError for unknown messages' do
      msg = ':penguin.omega.org.za NOTICE Auth :Welcome to Omega!'
      expect { Ircbot::Message.init(msg) }.to raise_error
    end

    it 'should raise an UnknownMessageError when an unknown code is received' do
      msg = ':penguin.omega.org.za 99999 juco :Current Global'
      expected_error = Ircbot::Message::UnknownMessageError
      expect { Ircbot::Message.init(msg) }.to raise_error expected_error
    end
  end

end
